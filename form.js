class Form {
    constructor (container, id, action, method, ...elements) {
        this._container = container;
        this._action = action;
        this._id = id;
        this._method = method;
        this._elem = elements;
        this._form = null;
    }

    render() {
        this._form = document.createElement('form');
        this._form.id = this._id;
        this._form.action = this._action;
        this._elem.forEach(element => {
            this._form.append(element.render())
        });
        this._form.addEventListener("submit", (e) => {
            e.preventDefault();
            const obj = this.serialize();

            const request = fetch(this._action, {
                method: this._method,
                headers: {


                    // "Date": "Sun, 08 Dec 2019 15:39:31 GMT",
                    "Server": "Apache",
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Methods": "GET, PUT, POST, DELETE, OPTIONS",
                    "Access-Control-Allow-Headers": "Authorization, Origin, X-Requested-With, Content-Type,      Accept",
                    "Keep-Alive": "timeout=5, max=100",
                    "Connection": "Keep-Alive",
                    "Transfer-Encoding": "chunked",



                    // "Access-Control-Allow-Origin": "*",
                    //         "Access-Control-Allow-Methods": "POST, GET, OPTIONS",
                    //         "Access-Control-Allow-Headers": "Authorization, Origin, X-Requested-With, Content-Type, Accept",
                    "Autorization": "Bearer ed735f69ec7d",
                    "Content-type" : "application/json"
                },
                body: JSON.stringify(obj)
            });

            console.log(JSON.stringify(obj));

            request.then(response => response.json())
                .then(result => {
                    if (result.status != "Success"){
                        const error = document.createElement('p');
                        error.textContent = result.message;
                        error.style.color = 'red';
                        this._form.append(error);
                    }


                });
        });
        this._container.append(this._form);
    }

    serialize () {
        const jsonObj = {};
        const inputs = this._form.querySelectorAll('input', 'select');
        inputs.forEach(input => {
            jsonObj[input.name] = input.value;
        });
        return jsonObj;
    }
}


class Fieldset {
    constructor (classList, display,  ...args) {
        this.classList = classList;
        this._display = display;
        this._args = args;
    }

    render () {
        const fieldset = document.createElement('fieldset');
        fieldset.classList = this.classList;
        fieldset.style.display = this._display;
        this._args.forEach( item => {
            fieldset.append( item.render() )
        });
        return fieldset;
    }
}


class Input {
    constructor (type="text", name="", id="", placeholder="", value="", required=false, ...classList) {
        this._type = type;
        this._name = name;
        this._id = id;
        this._placeholder = placeholder;
        this._value = value;
        this._required = required;
        this._classList = classList.join(" ");
        this.elem = null
    }

    render () {
        this.elem = document.createElement("input");
        this.elem.type = this._type;
        this.elem.name = this._name;
        this.elem.id = this._id;
        this.elem.placeholder  = this._placeholder;
        this.elem.value = this._value;
        this.elem.className = this._classList;
        if (this._required) {
            this.elem.required = true;
            this.validation();
        };
        return this.elem
    }

    validation () {
        this.elem.addEventListener('blur', () => {
            if ( !this.elem.value ) {
                const errorMessage = document.createElement('p');
                errorMessage.textContent = 'Please fill out this field';
                errorMessage.style.color = 'red';
                this.elem.after(errorMessage);
            }
        })
    }
}


class Select {
    constructor (name="", id="", required=false, text, className, ...options) {
        this._name = name;
        this._required = required;
        this._text = text;
        this._className = className;
        this._options = options;
        this._select = null;
    }


    render () {
        this._select = document.createElement('select');
        this._select.name = this._name;
        if ( this._required ) {
            this._select._required = true;
        }
        const optionDisabled = document.createElement('option');
        optionDisabled.textContent = this._text;
        this._select.className = this._className;
        this._select.append(optionDisabled);
        optionDisabled.setAttribute('disabled', '');

        this._options.forEach( item => {
            const option = document.createElement('option');
            option.value = item;
            option.text = item;
            this._select.append(option);
        });
        return this._select;
    }
}


class SelectInputs extends Select {
    constructor (...args ) {
        super(...args);
    }

    processingSelect () {
        this._select.addEventListener('change', function(e){
            const fieldset = document.querySelector('.'+this.value.toLowerCase()+'');
            const fieldsets = document.querySelectorAll('fieldset');
            fieldsets.forEach( item => item.style.display = 'none');
            var inputs = fieldset.querySelectorAll('input', 'select');
            if (fieldset.style.display = 'none') {
                fieldset.style.display = 'inline-block';
                // inputs.forEach(function(elem){elem.setAttribute('required','')})
            } else {
                fieldset.style.display = 'none';
                // inputs.forEach(function(elem){elem.removeAttribute('required')})
            }
        });
    }

    render () {
        super.render();
        this.processingSelect();
        return this._select;
    }
}




const root = document.getElementById('root');
const urlLogin = 'http://cards.danit.com.ua/login';
const urlCards = 'http://cards.danit.com.ua/cards';



/** Login inputs & Form */

const inputLogin = new Input('text', "login", "inputLogin", "login", "", "", "inline-block", "formInput");
const inputPassword = new Input('password', "password", "inputPassword", "Password", "", "", "inline-block",  "formInput");
const inputSubmit = new Input('submit', "submitLogin", "submitLogin", "", "Log In", "", "inline-block", "formSubmit");

const formAutorization = new Form(root, "loginForm", urlLogin, "POST", inputLogin, inputPassword, inputSubmit);
formAutorization.render();


/****** Doctor inputs & Form *****/

    // const selectDoctor = new Select("selectDoctor", "selectDoctor", true, "Chose specialist", "inline-block", "selectDoctor", "Cardiologist", "Dentist", "Therapist");
const selectDoctor = new SelectInputs("selectDoctor", "selectDoctor", true, "Chose specialist", "selectDoctor", "Cardiologist", "Dentist", "Therapist");
const inputSendForm = new Input('submit', "submitForm", "submitForm", "", "Create", "", "formSubmit");


/** Doctor cardiologist */

    // constructor (name="", id="", required=false, text, className, ...options) {
const cardiologistVisitTarget =      new Input('text', "visitTarget", "visitTarget", "Visit target", "", "", "formInput cardiologist");
const cardiologistVisitDescription = new Input('text', "visitTarget", "visitDescription", "Visit target", "", "", "formInput cardiologist");
const cardiologistSelectDoctor =     new Select("selectSpecialist", "selectDoctor", false, "Chose doctor", "cardiologist", "Vasia Pupkin", "Piotr Kardash", "Valera Serdechkin");
const cardiologistSelectUrgency =    new Select("selectSpecialist", "selectUrgency", false, "Chose urgency", "cardiologist", "Very fast", "Medium", "Soon");
const cardiologistStandartPressure = new Input('text', "standartPressure", "standartPressure", "Standart pressure", "", "", "formInput cardiologist");
const cardiologistBodyWeight =       new Input('text', "BodyWeight", "BodyWeight", "Body weight", "", "", "formInput cardiologist");
const cardiologistPreviousDiseases = new Input('text', "PreviousDiseases", "PreviousDiseases", "Previous diseases", "", "", "formInput cardiologist");
const cardiologistAge =              new Input('text', "age", "age", "Age", "", "", "none", "formInput cardiologist");
const cardiologistPatientName =      new Input('text', "patientName", "patientName", "patient name", "", "", "none", "formInput cardiologist");


/** Doctor Dentist */

const dentistVisitTarget =      new Input('text', "visitTarget", "visitTargetDentist", "Visit target", "", "", "formInput dentist");
const dentistVisitDescription = new Input('text', "visitTarget", "visitDescriptionDentist", "Visit target", "", "", "formInput dentist");
const dentistSelectDoctor =     new Select("selectSpecialist", "selectDoctorDentist", false, "Chose doctor", "dentist", "Vasia Zubkin", "Piotr Karies", "Valera Vurvizub");
const dentistSelectUrgency =    new Select("selectSpecialist", "selectUrgencyDentist", false, "Chose urgency", "dentist", "Very fast", "Medium", "Soon");
const dentistLastVisit =        new Input('date', "lastVisit", "lastVisitDentist", "Last visit", "", "", "formInput dentist");
const dentistPatientName =      new Input('text', "patientName", "patientNameDentist", "patient name", "", "", "formInput dentist");


/** Doctor Therapist */

const therapistVisitTarget =      new Input('text', "visitTarget", "visitTargetTherapist", "Visit target", "", "", "formInput therapist");
const therapistVisitDescription = new Input('text', "visitTarget", "visitDescriptionTherapist", "Visit target", "", "", "formInput therapist");
const therapistSelectDoctor =     new Select("selectSpecialist", "selectDoctorTherapist", false, "Chose doctor", "therapist", "Vasia Terrorist", "Piotr Terapist", "Valera Toporist");
const therapistSelectUrgency =    new Select("selectSpecialist", "selectUrgencyTherapist", false, "Chose urgency", "therapist", "Very fast", "Medium", "Soon");
const therapistPatientName =      new Input('text', "patientName", "patientNameTherapist", "patient name", "", "", "formInput therapist");


/** Create fieldsets for inputs*/

const fieldsetCardio = new Fieldset("cardiologist", "none", cardiologistVisitTarget,
    cardiologistVisitDescription, cardiologistSelectDoctor,
    cardiologistSelectUrgency, cardiologistStandartPressure,
    cardiologistBodyWeight, cardiologistPreviousDiseases,
    cardiologistAge, cardiologistPatientName);

const fieldsetDentist = new Fieldset("dentist", "none", dentistVisitTarget, dentistVisitDescription,
    dentistSelectDoctor, dentistSelectUrgency,
    dentistLastVisit, dentistPatientName);

const fieldsetTherapist = new Fieldset("therapist", "none", therapistVisitTarget, therapistVisitDescription,
    therapistSelectDoctor, therapistSelectUrgency, therapistPatientName);

/** Create Form */

const formVisit = new Form(root, "formVisit", urlCards, "POST", selectDoctor, fieldsetCardio, fieldsetDentist, fieldsetTherapist, inputSendForm);

formVisit.render();
